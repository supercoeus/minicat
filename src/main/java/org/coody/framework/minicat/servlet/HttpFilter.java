package org.coody.framework.minicat.servlet;

import org.coody.framework.minicat.entity.ApplicationFilterChain;
import org.coody.framework.minicat.entity.HttpServletRequest;
import org.coody.framework.minicat.entity.HttpServletResponse;

public abstract class HttpFilter {

	public abstract void doFilter(HttpServletRequest request,HttpServletResponse response,ApplicationFilterChain chain);
}
