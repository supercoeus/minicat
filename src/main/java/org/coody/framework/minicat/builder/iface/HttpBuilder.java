package org.coody.framework.minicat.builder.iface;

import java.io.IOException;
import java.text.MessageFormat;

import org.coody.framework.minicat.config.MiniCatConfig;
import org.coody.framework.minicat.entity.HttpServletRequest;
import org.coody.framework.minicat.entity.HttpServletResponse;
import org.coody.framework.minicat.exception.BadRequestException;
import org.coody.framework.minicat.exception.RequestNotInitException;
import org.coody.framework.minicat.exception.ResponseNotInitException;
import org.coody.framework.minicat.process.MinicatProcess;
import org.coody.framework.minicat.util.ByteUtils;
import org.coody.framework.minicat.util.GZIPUtils;
import org.coody.framework.minicat.util.StringUtil;

public abstract class HttpBuilder {

	protected HttpServletRequest request;

	protected HttpServletResponse response;

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public void buildResponse() throws IOException {
		if (response == null) {
			response = new HttpServletResponse();
		}
		buildResponse(response.getHttpCode(), response.getOutputStream().toByteArray());
	}

	public void buildResponse(byte[] data) throws IOException {
		if (response == null) {
			response = new HttpServletResponse();
		}
		buildResponse(response.getHttpCode(), data);
	}

	public void buildResponse(int httpCode, String msg) throws IOException {
		if (response == null) {
			response = new HttpServletResponse();
		}
		buildResponse(httpCode, msg.getBytes(MiniCatConfig.ENCODE));
	}

	public void buildResponse(int httpCode, byte[] data) throws IOException {
		if (response == null) {
			response = new HttpServletResponse();
		}
		buildResponseHeader();
		if (MiniCatConfig.OPENGZIP) {
			response.setHeader("Content-Encoding", "gzip");
			// 压缩数据
			data = GZIPUtils.compress(data);
		}
		Integer contextLength=0;
		if (data != null) {
			contextLength=data.length;
		}
		response.setHeader("Content-Length",contextLength.toString());
		StringBuilder responseHeader = new StringBuilder("HTTP/1.1 ").append(httpCode).append(" ").append("\r\n");
		for (String key : response.getHeaders().keySet()) {
			for (String header : response.getHeader(key)) {
				responseHeader.append(key).append(": ").append(header).append("\r\n");
			}
		}
		responseHeader.append("\r\n");
		response.getOutputStream().reset();
		response.getOutputStream().write(responseHeader.toString().getBytes(MiniCatConfig.ENCODE));
		if (!StringUtil.isNullOrEmpty(data)) {
			response.getOutputStream().write(data);
		}
	}

	public void buildResponseHeader() throws IOException {
		if (response == null) {
			throw new ResponseNotInitException("Response尚未初始化");
		}
		response.setHeader("Connection", "close");
		response.setHeader("Server", "MiniCat/1.0 By Coody");
		if (!response.containsHeader("Content-Type")) {
			response.setHeader("Content-Type", "text/html");
		}
		if (MiniCatConfig.OPENGZIP) {
			response.setHeader("Content-Encoding", "gzip");
		}
		if (request != null && request.isSessionCread()) {
			String cookie = MessageFormat.format("{0}={1}; HttpOnly", MiniCatConfig.SESSION_ID_FIELD_NAME,
					request.getSessionId());
			response.setHeader("Set-Cookie", cookie);
		}
	}

	public void buildRequestHeader() {
		if (request == null) {
			throw new RequestNotInitException("Request尚未初始化");
		}
		String line = ByteUtils.readLineString(request.getInputStream());
		if (StringUtil.isNullOrEmpty(line)) {
			throw new BadRequestException("错误的请求报文");
		}
		while (line.contains("  ")) {
			line = line.replace("  ", " ");
		}
		String[] vanguards = line.trim().split(" ");
		if (vanguards.length != 3) {
			throw new BadRequestException("错误的请求报文");
		}
		request.setMethod(vanguards[0]);
		String requestURI = vanguards[1];
		if (requestURI.contains("?")) {
			int index = requestURI.indexOf("?");
			if (index < requestURI.length() - 1) {
				request.setQueryString(requestURI.substring(index + 1));
			}
			requestURI = requestURI.substring(0, index);
		}
		request.setRequestURI(requestURI);
		request.setProtocol(vanguards[2]);
		line = ByteUtils.readLineString(request.getInputStream());
		if (StringUtil.isNullOrEmpty(line)) {
			throw new BadRequestException("错误的请求报文");
		}
		while (!StringUtil.isNullOrEmpty(line) && !line.equals("\r\n")) {
			int index = line.indexOf(":");
			if (index < 1) {
				throw new BadRequestException("错误的请求头部:" + line);
			}
			String name = line.substring(0, index).trim();
			String value = line.substring(index + 1).trim();
			if (StringUtil.hasNull(name, value)) {
				continue;
			}
			request.setHeader(name, value);
			if (name.equals("Content-Encoding")) {
				if (value.contains("gzip")) {
					request.setGzip(true);
				}
			}
			if (name.equals("Host")) {
				String basePath = request.getScheme() + "://" + value;
				if (requestURI.startsWith(basePath)) {
					requestURI = requestURI.substring(basePath.length());
				}
			}
			if (name.equals("Content-Length")) {
				request.setContextLength(Integer.valueOf(value));
			}
			line = ByteUtils.readLineString(request.getInputStream());
		}
	}

	private void destroy() {
		if (response != null && response.getOutputStream() != null) {
			try {
				response.getOutputStream().close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void builder() {
		try {
			buildRequest();
			buildRequestHeader();
			this.response = new HttpServletResponse();
			MinicatProcess.doService(this);
			buildResponse();
		} catch (BadRequestException e) {
			e.printStackTrace();
			try {
				buildResponse(400, "400 bad request");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
			try {
				buildResponse(500, "error execution");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				buildResponse(500, "error execution");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	public void flushAndClose() {
		try {
			flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			destroy();
		}
	}

	protected abstract void buildRequest() throws Exception;

	protected abstract void flush() throws IOException;


}
